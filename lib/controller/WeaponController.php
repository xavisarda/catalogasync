<?php

require_once(__DIR__.'/../model/db/WeaponDb.php');

/**
 * Clase controller
 * 
 * En este caso el controlador es una sola clase.
 */
class WeaponController{

    /**
     * Metodo para recuperar todos los registros del catlaogo
     * @return array listado de instancias de la herencia Weapon
     */
    public function listAction(){
        $dbs = new WeaponDb();
        return $dbs->list();
    }

    /**
     * Metodo para recuperar el detalle de un registro
     * @param string $idw id del registro en base de datos
     * @return Weapon instancia con los datos del registro
     */
    public function detailsAction($idw){
        $dbs = new WeaponDb();
        return $dbs->details($idw);
    }

    /**
     * Metodo para eliminar un registro
     * @param string $idw id del registro en base de datos
     * @return Weapon instancia con los datos del registro eliminado
     */
    public function deleteAction($idw){
      $dbs = new WeaponDb();
      return $dbs->delete($idw);
    }

    /**
     * Metodo para modificar un registro con los parametros proporcionados
     * @param string $idw id del registro en base de datos
     * @param int $t tipo de registro W_TYPE_NULL|W_TYPE_IMG|W_TYPE_YT|W_TYPE_SC
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $med identificador multimedia
     * @return Weapon instancia con los datos del registro modificado
     */
    public function updateAction($idw, $t, $n, $m, $o, $p, $f, $med){
      $dbs = new WeaponDb();
      return $dbs->udpate($idw, $t, $n, $m, $o, $p, $f, $med);
    }

    /**
     * Metodo para crear un registro con los parametros proporcionados
     * @param int $t tipo de registro W_TYPE_NULL|W_TYPE_IMG|W_TYPE_YT|W_TYPE_SC
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $med identificador multimedia
     * @return Weapon instancia con los datos del registro creado
     */
    public function createAction($t, $n, $m, $o, $p, $f, $med){
        $objret = null;
        $dbs = new WeaponDb();
        switch($t){
            case W_TYPE_NULL:
                $objret = $dbs->insertVoid($n, $m, $o, $p, $f);
                break;
            case W_TYPE_IMG:
                $objret = $dbs->insertImg($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_YT:
                $objret = $dbs->insertYT($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_SC:
                $objret = $dbs->insertSC($n, $m, $o, $p, $f, $med);
                break;
            default:
                return null;
        };
        return $objret;
    }

}
