<?php 

/**
 * Clase Weapon sin elementos multimedia
 */
class Weapon{
    
    private $_nombre;
    private $_filo;
    private $_origen;
    private $_material;
    private $_peso;
    private $_wid;
    
    /**
     * Constructor
     * 
     * @param string $n nombre del arma
     * @param string|int $f longitud del filo
     * @param string $o pais o region de origen
     * @param string $m material
     * @param string|float $p peso
     * @param string $wid id del registro en base de datos si existe
     */
    public function __construct($n, $f, $o, $m, $p, $wid = null){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setWid($wid);
    }
    
    public function getType(){
        return W_TYPE_NULL;
    }
    
    public function getNombre(){
        return $this->_nombre;
    }

    public function getFilo(){
        return $this->_filo;
    }

    public function getOrigen(){
        return $this->_origen;
    }

    public function getMaterial(){
        return $this->_material;
    }

    public function getPeso(){
        return $this->_peso;
    }
    
    public function getWid(){
        return $this->_wid;
    }
    
    public function setWid($_wid){
        $this->_wid = $_wid;
    }

    public function setNombre($_nombre){
        $this->_nombre = $_nombre;
    }

    public function setFilo($_filo){
        $this->_filo = $_filo;
    }

    public function setOrigen($_origen){
        $this->_origen = $_origen;
    }

    public function setMaterial($_material){
        $this->_material = $_material;
    }

    public function setPeso($_peso){
        $this->_peso = $_peso;
    }
    
    public function getView(){
        return __DIR__.'/../inc/weapon.php';
    }
    
    /**
     * Metodo toArray para convertir la instancia en 
     * un array asociativo.
     * 
     * @return array Array asociativo con los campos del registro
     */
    public function toArray(){
        $obj = array();
        $obj['nombre'] = $this->getNombre();
        $obj['filo'] = $this->getFilo();
        $obj['origen'] = $this->getOrigen();
        $obj['material'] = $this->getMaterial();
        $obj['peso'] = $this->getPeso();
        $obj['tipo'] = W_TYPE_NULL;
        if($this->getWid() != NULL){
            $obj['wid'] = $this->getWid();
        }
        
        return $obj;
    }
    
}