<?php 

require_once(__DIR__.'/Weapon.php');

/**
 * Clase WeaponImg con imagen
 * 
 * Hereda de Weapon
 * @see Weapon
 */
class WeaponImg extends Weapon{
    
    private $_imagen;
    
    /**
     * Constructor
     * 
     * @param string $n nombre del arma
     * @param string|int $f longitud del filo
     * @param string $o pais o region de origen
     * @param string $m material
     * @param string|float $p peso
     * @param string $i path o URL de la imagen
     * @param string $wid id del registro en base de datos si existe
     */
    public function __construct($n, $f, $o, $m, $p, $i, $wid = null){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setImagen($i);
        $this->setWid($wid);
    }
    
    public function getType(){
        return W_TYPE_IMG;
    }
    
    public function getImagen(){
        return $this->_imagen;
    }

    public function setImagen($_imagen){
        $this->_imagen = $_imagen;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponImg.php';
    }
    
    /**
     * Metodo toArray para convertir la instancia en 
     * un array asociativo.
     * 
     * @return array Array asociativo con los campos del registro
     */
    public function toArray(){
        $obj = array();
        $obj['nombre'] = $this->getNombre();
        $obj['filo'] = $this->getFilo();
        $obj['origen'] = $this->getOrigen();
        $obj['material'] = $this->getMaterial();
        $obj['peso'] = $this->getPeso();
        $obj['media1'] = $this->getImagen();
        $obj['tipo'] = W_TYPE_IMG;
        if($this->getWid() != NULL){
            $obj['wid'] = $this->getWid();
        }
        
        return $obj;
    }
    
}