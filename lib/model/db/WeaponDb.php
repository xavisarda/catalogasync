<?php


require_once(__DIR__.'/../Weapon.php');
require_once(__DIR__.'/../WeaponImg.php');
require_once(__DIR__.'/../WeaponSC.php');
require_once(__DIR__.'/../WeaponYT.php');

/**
 * Classe adapter para la conexion y la ejecucion de consultas
 * a la base de datos.
 * 
 * Esta clase aglutina todas las consultas de la herencia de Weapon
 * @see Weapon
 */
class WeaponDb{

    private $_conn;

    /**
     * Metodo para la insercion de una arma sin elementos multimedia.
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @return Weapon instancia del arma creada
     */
    public function insertVoid($n, $m, $o, $p, $f){
        $obj = new Weapon($n, $f, $o, $m, $p);
        return $this->insert($obj);
    }

    /**
     * Metodo para la insercion de una arma con imagen.
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $img path o URL de la imagen
     * @return Weapon instancia del arma creada
     */
    public function insertImg($n, $m, $o, $p, $f, $img){
        $obj = new WeaponImg($n, $f, $o, $m, $p, $img);
        return $this->insert($obj);
    }

    /**
     * Metodo para la insercion de una arma con video de YouTube.
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $yt identificador alfanumerico del video de YT
     * @return Weapon instancia del arma creada
     */
    public function insertYT($n, $m, $o, $p, $f, $yt){
        $obj = new WeaponYT($n, $f, $o, $m, $p, $yt);
        return $this->insert($obj);
    }

    /**
     * Metodo para la insercion de una arma con pista de SoundCloud.
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $sc identificador alfanumerico del audio de SC
     * @return Weapon instancia del arma creada
     */
    public function insertSC($n, $m, $o, $p, $f, $sc){
        $obj = new WeaponSC($n, $f, $o, $m, $p, $sc);
        return $this->insert($obj);
    }

    /**
     * Metodo helper que crea el registro en la base de datos.
     * Fijate que el metodo es privado y 
     * no se puede llamar desde fuera de la clase.
     * 
     * @param Weapon $wObject instancia del arma para crear el nuevo registro
     * return Weapon instancia del arma creada
     */
    private function insert($wObject){
        $collection = $this->getCollection();
        $insertOneResult = $collection->insertOne($wObject->toArray());

        $idIns = $insertOneResult->getInsertedId()->__toString();
        return $this->details($idIns);
    }

    /**
     * Metodo para la modificación de una arma.
     * @param string $idarma identificador del arma en la base de datos
     * @param string $t tipo de arma W_TYPE_NULL|W_TYPE_IMG|W_TYPE_YT|W_TYPE_SC
     * @param string $n nombre del arma
     * @param string $m material
     * @param string $o pais o region de origen
     * @param string|float $p peso
     * @param string|int $f longitud del filo
     * @param string $med identificador multimedia
     * @return Weapon instancia del arma modificada
     */
    public function update($idarma, $t, $n, $m, $o, $p, $f, $med){
      $collection = $this->getCollection();
      $setupdate = array();
      $setupdate['nombre'] = $n;
      $setupdate['tipo'] = $t;
      $setupdate['material'] = $m;
      $setupdate['origen'] = $o;
      $setupdate['peso'] = $p;
      $setupdate['filo'] = $f;

      //creamos un campo u otro en funcion del tipo de registro a modificar
      switch ($t) {
        case W_TYPE_NULL:
            break;
        case W_TYPE_IMG:
            $setupdate['media1'] = $med;
            break;
        case W_TYPE_YT:
            $setupdate['media2'] = $med;
            break;
        case W_TYPE_SC:
            $setupdate['media3'] = $med;
            break;
      }

      $insertOneResult = $collection->update(
        ["_id" => new MongoDB\BSON\ObjectId($idarma)],
        [ '$set' => $setupdate ]);

      return $this->details($idarma);
    }

    /**
     * Metodo para la eliminación de un registro.
     * @param string $idarma identificador del arma en la base de datos
     * @return Weapon instancia del registro eliminado
     */
    public function delete($idarma){
      $collection = $this->getCollection();
      //Primero recuperamos la informacion del registro a eliminar
      $w = $collection->findOne(
          ["_id" => new MongoDB\BSON\ObjectId($idarma)]);

      $wo = $this->createWpnObject($w);

      //Eliminamos el registro
      $collection->deleteOne(["_id" => new MongoDB\BSON\ObjectId($idarma)]);

      //Devolvemos la información del registro eliminado
      return $wo;
    }

    /**
     * Metodo para la consulta de todos los registros de la base de datos.
     * @return array listado de instancias de la herencia Weapon
     */
    public function list(){
        $collection = $this->getCollection();
        $list = $collection->find();

        $wlist = array();
        foreach ($list as $wpmdb){
            $wpn = $this->createWpnObject($wpmdb);
            array_push($wlist,$wpn);
        }
        return $wlist;
    }

    /**
     * Metodo para la consulta de un registro por su id.
     * @param string $idarma identificador del arma en la base de datos
     * @return Weapon instancia del registro consultado
     */
    public function details($idarma){
        $collection = $this->getCollection();
        $w = $collection->findOne(
            ["_id" => new MongoDB\BSON\ObjectId($idarma)]);

        $wo = $this->createWpnObject($w);

        return $wo;
    }

    /**
     * Metodo helper privado para la creacion de una instancia de la herencia 
     * de Weapon a traves de un array asociativo.
     * 
     * @return Weapon instancia de la herencia de Weapon
     */
    private function createWpnObject($arma){
        $armaO = null;
        //Creamos la instancia de una clase u otra en funcion del tipo de registro
        switch($arma['tipo']){
            case W_TYPE_NULL:
                $armaO = new Weapon($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso'],
                $arma['_id']->__toString());
                break;
            case W_TYPE_IMG:
                $armaO = new WeaponImg($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media1'], $arma['_id']->__toString());
                break;
            case W_TYPE_YT:
                $armaO = new WeaponYT($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media2'], $arma['_id']->__toString());
                break;
            case W_TYPE_SC:
                $armaO = new WeaponSC($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media3'], $arma['_id']->__toString());
                break;
        };
        return $armaO;
    }

    /**
     * Metodo helper privado para la conexion a la base de datos
     * y seleccion de la coleccion.
     * @return Collection coleccion de la base de datos con la que interactuar
     */
    private function getCollection(){
       return (new MongoDB\Client)->wdb->wpns;
    }

}
