<?php

/* Configuramos los nombres de las clases que vamos a utilizar */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

/* Requerimos autoload, constantes y el controlador */
require_once(__DIR__.'/../ext/vendor/autoload.php');
require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/WeaponController.php');

/* Creamos la aplicacion Slim a traves del factory */
$app = AppFactory::create();
$app->addRoutingMiddleware();

/* Configuracion de la respuesta para requests GET con path /weapons 
 * Esta request devuelve un listado los registros disponibles en la base de datos.
 */
$app->get('/weapons',function( Request $request, Response $response, array $args) {
    $cnt = new WeaponController();
    $wps = $cnt->listAction();

    $arrayW = array();
    foreach ($wps as $wp) {
      $arrayW[] = $wp->toArray();
    }

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($arrayW));
    return $newR;
});

/* Configuracion de la respuesta para requests GET con path /weapons/idweapon 
 * Esta request devuelve el detalle del registro con la id definida en la URL de la 
 * peticion.
 */
$app->get('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->detailsAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

/* Configuracion de la respuesta para requests POST con el path /weapons 
 * Esta request crea un nuevo registro con los datos que llegan en el cuerpo de 
 * la peticion.
 */
$app->post('/weapons',function(Request $request, Response $response, array $args) {
    $wjson = $request->getParsedBody();
    $cnt = new WeaponController();

    $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
        $wjson['material'], $wjson['origen'], $wjson['peso'],
        $wjson['filo'], $wjson['media']);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($newwp->toArray()));
    return $newR;
});

/* Configuracion de la respuesta para requests DELETE con path /weapons/idweapon 
 * Esta request elimina el registro con la id definida en la URL de la peticion y devuelve 
 * los datos que habia en el registro.
 */
$app->delete('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

/* Configuracion de la respuesta para requests PUT con path /weapons/idweapon 
 * Esta request modifica el registro con la id definida en la URL de la peticion con los datos 
 * que llegan en el cuerpo de la peticion y devuelve el resultado de la actualizacion.
 */
$app->put('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $wjson = $request->getParsedBody();
  $cnt = new WeaponController();

  $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
      $wjson['material'], $wjson['origen'], $wjson['peso'],
      $wjson['filo'], $wjson['media']);

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

/* Ejecutamos la aplicacion creada una vez configuradas todas las duplas 
 * verbo HTTP / path
 */
$app->run();
