
/* 
 * Esta funcion esconde los elementos del formulario de modificacion.
 */
function noUpdate(){
  $('#updateButton').hide();
  $('#createformw').hide();
}

/* 
 * Esta funcion muestra el campo multimedia en funcion del tipo de registro.
 */
function hideShowMedia(){
  var typeW = $('#fwtype').val();
  if(typeW == 0){
	  $('.mweapon').hide();
  }else if(typeW == 1){
	  $('.mweapon').show();
	  $('#mediatitle').html('Imagen');
  }else if(typeW == 2){
	  $('.mweapon').show();
	  $('#mediatitle').html('YouTube Id');
  }else if(typeW == 3){
	  $('.mweapon').show();
	  $('#mediatitle').html('SoundCloud Id');
  }
}

/*
 * Esta funcion envia una peticion al servidor para obtener el listado
 * de registros y los muestra en contenedor HTML con ID wlist.
 */
function listWeapons(){
	$.ajax({
		url:"/weapons",
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		var htmlw = '';
		$.each(response, function(index,value){
			htmlw += '<div>';
			htmlw += '<h3><span>'+value.nombre+'</span></h3>';
			htmlw += '<p>Peso: '+value.peso+'</p>';
			htmlw += '<p>Filo: '+value.filo+'</p>';
			htmlw += '<p><span onclick="detailsW(\''+value.wid+'\')">View</span></br>';
          htmlw += '<span onclick="updateFormW(\''+value.wid+'\')">Update</span></br>';
			htmlw += '<span onclick="deleteW(\''+value.wid+'\')">Delete</span></p>';
			htmlw += '</div>';
		});
	$('#wlist').html(htmlw);

	});
}

/*
 * Esta funcion envia una peticion al servidor para obtener el detalle
 * de un registro y los muestra en contenedor HTML con ID wdetail.
 */
function detailsW(weapon){
	$.ajax({
		url:"/weapons/"+weapon,
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		var htmlw = '';
			htmlw += '<div>';
			htmlw += '<h3><span>'+response.nombre+'</span></h3>';
			htmlw += '<p>Peso: '+response.peso+'</p>';
			htmlw += '<p>Filo: '+response.filo+'</p>';
			if(response.tipo == 1){
				htmlw += '<p><img src="'+response.media1+'"/></p>';
			}else if(response.tipo == 2){
				htmlw += '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+response.media2+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			}else if(response.tipo == 3){
				//htmlw += '<p><img src="'+response.media1+'"/></p>';
			}
			htmlw += '</div>';
	$('#wdetail').html(htmlw);
	});
}

/*
 * Esta funcion envia una peticion al servidor para eliminar el
 * registro con la id definida en la URL y refresca el listado de registros.
 */
function deleteW(weapon){
	$.ajax({
		url:"/weapons/"+weapon,
		method:"delete",
		accept:"json",
		dataType: "json"
	}).done(function(response){
		listWeapons();
	});
}

/*
 * Esta funcion carga los campos del registro a modificar en el formulario 
 * disponible en la pagina principal.
 */
function updateFormW(weapon){
  $('#createButton').hide();
  $('#updateButton').show();
  $('#createformw').show();
  $.ajax({
		url:"/weapons/"+weapon,
		method:"get",
		accept:"json",
		dataType: "json"
	}).done(function(response){
    $("#wfname").val(response.nombre);
    $("#wffil").val(response.filo);
    $("#wforig").val(response.origen);
    $("#wfmat").val(response.material);
    $("#wfweig").val(response.peso);
    $("#fwtype").val(response.tipo);
    hideShowMedia();
    if(response.tipo == 1){
      $("#wfmedia").val(response.media1);
    }else if(response.tipo == 2){
  	  $("#wfmedia").val(response.media2);
    }else if(response.tipo == 3){
  	  $("#wfmedia").val(response.media3);
    }
    $("#wfidup").val(response.wid);
	});

}

/*
 * Esta funcion envia una peticion al servidor para modificar el
 * registro con la id definida en la URL y los datos en el cuerpo
 * de la peticion y refresca el listado de registros.
 */
function updateWeapon(){
	var wp = { "nombre" : $("#wfname").val(),
			"filo" : $("#wffil").val(),
			"origen" : $("#wforig").val(),
			"material" : $("#wfmat").val(),
			"peso" : $("#wfweig").val(),
			"tipo" : $("#fwtype").val(),
			"media" : $("#wfmedia").val(),
			};
  var wid = $("#wfidup").val();

	$.ajax({
		url:"/weapons/"+wid,
		method:"put",
		accept:"json",
		data : wp,
		dataType: "json"
	}).done(function(response){
		createFormW();
      listWeapons();
	});
}

/*
 * Esta funcion limpia los campos del formulario para convertirlo
 * en un formulario de creacion de registros en lugar de uno de 
 * modificacion.
 */
function createFormW(){
  $("#wfname").val('');
  $("#wffil").val('');
  $("#wforig").val('');
  $("#wfmat").val('');
  $("#wfweig").val('');
  $("#fwtype").val(0);
  hideShowMedia();
  $("#wfmedia").val('');
  $("#wfidup").val('');
  $('#createButton').show();
  $('#updateButton').hide();
  $('#createformw').hide();
}

/*
 * Esta funcion envia una peticion al servidor para crear un
 * nuevo registro con los datos en el cuerpo de la peticion 
 * y refresca el listado de registros.
 */
function createWeapon(){
	var wp = { "nombre" : $("#wfname").val(),
			"filo" : $("#wffil").val(),
			"origen" : $("#wforig").val(),
			"material" : $("#wfmat").val(),
			"peso" : $("#wfweig").val(),
			"tipo" : $("#fwtype").val(),
			"media" : $("#wfmedia").val()
			};

	$.ajax({
		url:"/weapons",
		method:"post",
		accept:"json",
		data : wp,
		dataType: "json"
	}).done(function(response){
		createFormW();
		listWeapons();
	});
}
