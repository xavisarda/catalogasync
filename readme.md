# Aplicación de Catálogo asíncrono
Este proyecto es solamente una muestra orientada a la formación para la implementación de APIs REST y comunicación asíncrona con jQuery.

La aplicación no tiene estilo aplicados, solo se ofrece para practicar la interacción asíncrona cliente-servidor.

## Requisitos técnicos
Para que esta aplicación funcione se debe tener un servidor web con la redirección activadas. En el caso de Apache2 los pasos para que esto sea posible son:

* Tener el módulo rewrite instalado y activado. En Ubuntu 18.04
~~~
sudo a2enmode rewrite
~~~ 
* Que el directorio raíz del virutal host tenga la directiva **AllowOverride** con valor **All**. Para Apache2:
~~~
<Directory /path/to/documentroot>
        AllowOverride All
</Directory>
~~~ 
* Mantener el fichero .htaccess de la carpeta public en el despliegue. Si utilizáis el que se recomienda desde la página de instalación de Slim Framework no funcionará, ya que en este proyecto **index.html** actúa como vista del proyecto y el resto de peticiones se redireccionan a **api.php**. Por ese motivo en este proyecto, el fichero .htaccess tiene el siguiente contenido:
~~~
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ api.php [QSA,L]
~~~ 
* Tener instalado un servidor de MongoDB y el driver de bajo nivel de MongoDB para PHP. Sin el driver, la librería no podrá conectar con el servidor de MongoDB. Para este proyecto se han utilizado las versiones que podéis consultar a continuación.

## Librerías utilizadas

+ **Apache2** versión 2.4.29 - Instalado desde los repositorios de Ubuntu 18.04
+ **mod_rewrite** activado.
+ **PHP** versión 7.2.24 - Instalado desde los repositorios de Ubuntu 18.04 y actuando como módulo de Apache.
+ **MongoDB** versión 4.2.6 - Instalado desde los [repositorios oficiales de MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
+ **Driver de MongoDB** versión 1.7.4 - Compliado y enlazado en el servidor (PECL).
+ **Slim Framework** versión 4.5 - Instalado a través de Composer en el directorio _ext_ y con el módulo de _slim/psr7_.
+ **Librería MongoDB** versión 1.6 - Instalado a través de Composer (no funcionará si el driver no se encuentra instalado y activado tanto para el servidor web como para la interfaz de linea de comando o CLI).

## Aviso
No se utilizan tildes en los comentarios dentro del código para evitar problemas de codificación.